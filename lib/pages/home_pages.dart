import 'package:bwa_real_cozy/models/city.dart';
import 'package:bwa_real_cozy/models/space.dart';
import 'package:bwa_real_cozy/models/tips.dart';
import 'package:bwa_real_cozy/providers/space_provider.dart';
import 'package:bwa_real_cozy/theme.dart';
// import 'package:bwa_real_cozy/widgets/bottom_navbar_item.dart';
import 'package:bwa_real_cozy/widgets/city_card.dart';
import 'package:bwa_real_cozy/widgets/space_card.dart';
// import 'package:bwa_real_cozy/widgets/space_card.dart';
import 'package:bwa_real_cozy/widgets/space_card_slider.dart';
import 'package:bwa_real_cozy/widgets/tips_card.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int index;
  List<String> gambar = [
    'https://images.unsplash.com/photo-1501183638710-841dd1904471?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MXx8aG9tZXxlbnwwfHwwfHw%3D&auto=format&fit=crop&w=500&q=60',
    'https://images.unsplash.com/photo-1560184897-67f4a3f9a7fa?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1171&q=80',
    'https://images.unsplash.com/photo-1560448204-e02f11c3d0e2?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80',
    'https://images.unsplash.com/photo-1516455590571-18256e5bb9ff?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80',
    'https://images.unsplash.com/photo-1554995207-c18c203602cb?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80',
  ];
  @override
  Widget build(BuildContext context) {
    var spaceProvider = Provider.of<SpaceProvider>(context);
    spaceProvider.getSpace();

    return Scaffold(
      // floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      // floatingActionButton: Container(
      //   width: MediaQuery.of(context).size.width - (2 * 18),
      //   height: 60,
      //   decoration: BoxDecoration(
      //     borderRadius: BorderRadius.all(
      //       Radius.circular(23),
      //     ),
      //     color: Color(0xFFF6F7F8),
      //     boxShadow: [
      //       BoxShadow(
      //         color: Colors.grey.withOpacity(0.7),
      //         spreadRadius: 2,
      //         blurRadius: 10,
      //         offset: Offset(0, 5), // changes position of shadow
      //       ),
      //     ],
      //   ),
      //   child: Row(
      //     mainAxisAlignment: MainAxisAlignment.spaceAround,
      //     children: [
      //       BottomNavbarItemNew(
      //         imageUrl: 'assets/Icon_home_solid.png',
      //         isActive: true,
      //       ),
      //       BottomNavbarItemNew(
      //         imageUrl: 'assets/Icon_mail_solid.png',
      //         isActive: false,
      //       ),
      //       BottomNavbarItemNew(
      //         imageUrl: 'assets/Icon_card_solid.png',
      //         isActive: false,
      //       ),
      //       BottomNavbarItemNew(
      //         imageUrl: 'assets/Icon_love_solid.png',
      //         isActive: false,
      //       ),
      //     ],
      //   ),
      // ),
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.symmetric(
            horizontal: 24,
          ),
          child: ListView(
            // NOTE: Header / Title
            children: [
              SizedBox(
                height: 24,
              ),
              Row(
                children: [
                  Text(
                    'Recomended House',
                    style: blackTextStyle.copyWith(
                      fontSize: 24,
                    ),
                  ),
                  Spacer(),
                  Image.asset(
                    'assets/new_logo.png',
                    width: 50,
                    height: 50,
                  ),
                ],
              ),
              SizedBox(
                height: 2,
              ),
              Text(
                'Mencari rumah yang cozy',
                style: greyTextStyle.copyWith(
                  fontSize: 16,
                ),
              ),
              SizedBox(
                height: 20,
              ),
              // FutureBuilder(
              //   future: spaceProvider.getSpace(),
              //   builder: (context, snapshot) {
              //     if (snapshot.hasData) {
              //       List<Space> data = snapshot.data;
              //       return CarouselSlider(
              //         options: CarouselOptions(
              //           height: 200.0,
              //           autoPlay: true,
              //           autoPlayInterval: Duration(seconds: 3),
              //         ),
              //         items: data.map((item) {
              //           return SpaceCard(item);
              //         }).toList(),
              //       );
              //     } else {
              //       return Center(
              //         child: CircularProgressIndicator(
              //           color: purpleColor,
              //         ),
              //       );
              //     }
              //   },
              // ),
              FutureBuilder(
                future: spaceProvider.getSpace(),
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    List<Space> data = snapshot.data;
                    return Column(
                      children: [
                        CarouselSlider(
                          options: CarouselOptions(
                            onPageChanged: (i, reason) {
                              setState(() {
                                index = i + 1;
                                print(index);
                              });
                            },
                            height: 200.0,
                            autoPlay: true,
                            aspectRatio: 4 / 3,
                            autoPlayInterval: Duration(seconds: 3),
                            autoPlayAnimationDuration:
                                Duration(milliseconds: 800),
                            enlargeCenterPage: true,
                          ),
                          items: data.map((item) {
                            return SpaceCardSlider(item);
                          }).toList(),
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: data.map(
                            (index2) {
                              return AnimatedContainer(
                                duration: Duration(milliseconds: 500),
                                width: index2.id == index ? 50.0 : 8.0,
                                height: 8.0,
                                margin: EdgeInsets.symmetric(
                                    vertical: 10.0, horizontal: 2.0),
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10),
                                  // shape: BoxShape.circle,
                                  color: index2.id == index
                                      ? greenColor
                                      : Color.fromRGBO(0, 0, 0, 0.4),
                                ),
                              );
                            },
                          ).toList(),
                        ),
                      ],
                    );
                  } else {
                    return Center(
                      child: CircularProgressIndicator(
                        color: greenColor,
                      ),
                    );
                  }
                },
              ),
              // CarouselSlider(
              //     options: CarouselOptions(
              //       onPageChanged: (i, reason) {
              //         setState(() {
              //           index = i;
              //           // print(gambar[index]);
              //         });
              //       },
              //       height: 200.0,
              //       autoPlay: true,
              //       aspectRatio: 4 / 3,
              //       autoPlayInterval: Duration(seconds: 3),
              //       autoPlayAnimationDuration: Duration(milliseconds: 800),
              //       enlargeCenterPage: true,
              //     ),
              //     items: [1, 2, 3, 4, 5].map((i) {
              //       return GestureDetector(
              //         onTap: () {
              //           print(gambar[i - 1]);
              //         },
              //         child: Builder(
              //           builder: (BuildContext context) {
              //             return SpaceCardSlider(gambar[i - 1]);
              //           },
              //         ),
              //       );
              //     }).toList(),
              //   ),
              // Row(
              //   mainAxisAlignment: MainAxisAlignment.center,
              //   children: [1, 2, 3, 4, 5].map(
              //     (index2) {
              //       return Container(
              //         width: 8.0,
              //         height: 8.0,
              //         margin:
              //             EdgeInsets.symmetric(vertical: 10.0, horizontal: 2.0),
              //         decoration: BoxDecoration(
              //           shape: BoxShape.circle,
              //           color: (index2 - 1) == index
              //               ? greenColor
              //               : Color.fromRGBO(0, 0, 0, 0.4),
              //         ),
              //       );
              //     },
              //   ).toList(),
              // ),
              SizedBox(
                height: 20,
              ),
              // NOTE: Konten Popular
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    'Popular Cities',
                    style: regularTextStyle.copyWith(
                      fontSize: 16,
                    ),
                  ),
                  Text(
                    'view more >',
                    style: regularTextStyle.copyWith(
                      fontSize: 14,
                      color: greyColor,
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 16,
              ),
              Container(
                height: 150,
                child: ListView(
                  scrollDirection: Axis.horizontal,
                  children: [
                    CityCard(
                      City(
                        id: 1,
                        imageUrl: 'assets/city1.png',
                        name: 'Jakarta',
                        isPopular: true,
                      ),
                    ),
                    SizedBox(
                      width: 20,
                    ),
                    CityCard(
                      City(
                        id: 2,
                        imageUrl: 'assets/city2.png',
                        name: 'Bandung',
                        isPopular: false,
                      ),
                    ),
                    SizedBox(
                      width: 20,
                    ),
                    CityCard(
                      City(
                        id: 1,
                        imageUrl: 'assets/city3.png',
                        name: 'Surabaya',
                        isPopular: false,
                      ),
                    ),
                    SizedBox(
                      width: 20,
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 30,
              ),
              // // NOTE: Konten Rekomended
              // Text(
              //   'Recomended Space',
              //   style: regularTextStyle.copyWith(
              //     fontSize: 16,
              //   ),
              // ),
              // SizedBox(
              //   height: 16,
              // ),
              // FutureBuilder(
              //   future: spaceProvider.getSpace(),
              //   builder: (context, snapshot) {
              //     if (snapshot.hasData) {
              //       List<Space> data = snapshot.data;
              //       return Column(
              //         children: data.map((item) {
              //           return Container(
              //             margin: EdgeInsets.only(
              //               bottom: 30,
              //             ),
              //             child: SpaceCard(item),
              //           );
              //         }).toList(),
              //       );
              //     } else {
              //       return Center(
              //         child: CircularProgressIndicator(
              //           color: purpleColor,
              //         ),
              //       );
              //     }
              //   },
              // ),
              Text(
                'Tips & Guidance',
                style: regularTextStyle.copyWith(
                  fontSize: 16,
                ),
              ),
              SizedBox(
                height: 16,
              ),
              Column(
                children: [
                  TipsCard(
                    Tips(
                      id: 1,
                      imageUrl: 'assets/tips4.png',
                      title: 'City Guildness',
                      updatedAt: '20 Apr',
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  TipsCard(
                    Tips(
                      id: 2,
                      imageUrl: 'assets/tips3.png',
                      title: 'Jakarta Fairship',
                      updatedAt: '11 Dec',
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 70,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
