class Space {
  int id;
  String name;
  String imageUrl;
  int price;
  String city;
  String country;
  int rating;
  String address;
  String mapUrl;
  int numberOfKitchens;
  int numberOfKCupboards;
  int numberOfBedRooms;
  String phone;
  List photos;

  Space({
    this.id,
    this.name,
    this.imageUrl,
    this.price,
    this.city,
    this.country,
    this.rating,
    this.address,
    this.mapUrl,
    this.numberOfBedRooms,
    this.numberOfKCupboards,
    this.numberOfKitchens,
    this.phone,
    this.photos,
  });

  Space.fromJson(json) {
    id = json['id'];
    name = json['name'];
    imageUrl = json['image_url'];
    price = json['price'];
    city = json['city'];
    country = json['country'];
    rating = json['rating'];
    address = json['address'];
    mapUrl = json['map_url'];
    numberOfBedRooms = json['number_of_bedrooms'];
    numberOfKCupboards = json['number_of_cupboards'];
    numberOfKitchens = json['number_of_kitchens'];
    phone = json['phone'];
    photos = json['photos'];
  }
}
