import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:bwa_real_cozy/models/space.dart';
import 'package:http/http.dart' as http;

class SpaceProvider extends ChangeNotifier {
  getSpace() async {
    var hasil =
        await http.get('https://bwa-cozy.herokuapp.com/recommended-spaces');

    if (hasil.statusCode == 200) {
      List data = jsonDecode(hasil.body);
      List<Space> spaces = data.map((item) => Space.fromJson(item)).toList();
      return spaces;
    } else {
      return <Space>[];
    }
  }
}
