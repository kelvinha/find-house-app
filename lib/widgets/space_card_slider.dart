import 'package:bwa_real_cozy/models/space.dart';
import 'package:flutter/material.dart';

import '../theme.dart';

class SpaceCardSlider extends StatelessWidget {
  final Space space;
  // final String url;
  // SpaceCardSlider(this.url, this.space);
  SpaceCardSlider(this.space);
  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(
        30,
      ),
      child: Container(
        width: MediaQuery.of(context).size.width,
        margin: EdgeInsets.symmetric(horizontal: 3),
        child: Stack(
          children: [
            Image.network(
              space.imageUrl,
              width: MediaQuery.of(context).size.width,
              height: 200.0,
              fit: BoxFit.cover,
            ),
            Align(
              alignment: Alignment.bottomCenter,
              child: Container(
                padding: EdgeInsets.symmetric(
                  horizontal: 10,
                ),
                color: Colors.black.withOpacity(0.4),
                height: 50,
                width: MediaQuery.of(context).size.width,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          space.name,
                          style: blackTextStyle.copyWith(
                            color: whiteColor,
                          ),
                        ),
                        Text(
                          "${space.city}, ${space.country}",
                          style: greyTextStyle.copyWith(
                            color: whiteColor,
                            fontSize: 14,
                          ),
                        ),
                      ],
                    ),
                    Text.rich(
                      TextSpan(
                        text: '\$${space.price} ',
                        style: greenTextStyle.copyWith(
                          fontSize: 16,
                        ),
                        children: [
                          TextSpan(
                            text: '/ Month',
                            style: whiteTextStyle.copyWith(
                              fontSize: 16,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Align(
              alignment: Alignment.topRight,
              child: Container(
                height: 30,
                width: 100,
                decoration: BoxDecoration(
                  color: Color(0xFF00cf60),
                  borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(30),
                  ),
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Image.asset(
                      'assets/Icon_star_solid.png',
                      width: 18,
                    ),
                    Text(
                      "${space.rating}/5",
                      style: whiteTextStyle.copyWith(
                        fontSize: 13,
                      ),
                    )
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
