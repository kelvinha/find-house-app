import 'package:bwa_real_cozy/theme.dart';
import 'package:flutter/material.dart';

class RatingItem extends StatelessWidget {
  final int index;
  final int rating;

  RatingItem({this.index, this.rating});

  @override
  Widget build(BuildContext context) {
    return index <= rating
        ? Image.asset(
            'assets/Icon_star_solid.png',
            width: 20,
          )
        : Image.asset(
            'assets/Icon_star_solid.png',
            width: 20,
            color: greyColor,
          );
  }
}
