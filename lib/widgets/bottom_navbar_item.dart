import 'package:bwa_real_cozy/theme.dart';
import 'package:flutter/material.dart';

class BottomNavbarItemNew extends StatelessWidget {
  final String imageUrl;
  final bool isActive;

  BottomNavbarItemNew({this.imageUrl, this.isActive});
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Spacer(),
        Image.asset(
          imageUrl,
          width: 26,
        ),
        Spacer(),
        isActive
            ? Container(
                width: 30,
                height: 2,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.vertical(
                    top: Radius.circular(1000),
                  ),
                  color: purpleColor,
                ),
              )
            : Container(),
      ],
    );
  }
}
